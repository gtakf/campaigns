package com.alictus.campaigns.domain;

import lombok.*;

import java.io.File;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ExcelFile {

    private String filename;
    private String mimeType;
    private long size;
    private File file;
    private Map<Integer, List<String>> cells;

}
