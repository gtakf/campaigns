package com.alictus.campaigns.domain;

import lombok.*;

import java.math.BigDecimal;
import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Campaign {

    private String campaignName;
    private BigInteger totalImpression;
    private BigInteger totalClicks;
    private BigDecimal clickRate;
    private BigDecimal costPerClick;
    private BigInteger totalAppInstall;
    private BigDecimal totalBudget;

}
