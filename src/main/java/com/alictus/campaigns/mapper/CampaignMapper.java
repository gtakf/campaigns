package com.alictus.campaigns.mapper;

import com.alictus.campaigns.constant.FileConstants;
import com.alictus.campaigns.domain.Campaign;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class CampaignMapper {

    public static final CampaignMapper INSTANCE = Mappers.getMapper(CampaignMapper.class);

    public Campaign toCampaignFromSpreadSheet(List<Object> sheetRowValues) {
        return new Campaign(String.valueOf(sheetRowValues.get(0)),
                new BigInteger(String.valueOf(sheetRowValues.get(1))),
                new BigInteger(String.valueOf(sheetRowValues.get(2))),
                new BigDecimal(String.valueOf(sheetRowValues.get(3)).replace(",", ".")),
                new BigDecimal(String.valueOf(sheetRowValues.get(4)).replace(",", ".")),
                new BigInteger(String.valueOf(sheetRowValues.get(5))),
                new BigDecimal(String.valueOf(sheetRowValues.get(6)).replace(",", ".")));
    }

    public Campaign toCampaignFromCells(List<String> cells) {
        return new Campaign(cells.get(0), new BigInteger(cells.get(1).split(FileConstants.DOT_SPLIT)[0]),
                new BigInteger(cells.get(2).split(FileConstants.DOT_SPLIT)[0]),
                new BigDecimal(cells.get(3)), new BigDecimal(cells.get(4)),
                new BigInteger(cells.get(5).split(FileConstants.DOT_SPLIT)[0]), BigDecimal.ZERO);
    }

    public List<Object> toSpreadSheetsRowFromCampaign(Campaign campaign) {
        List<Object> list = new ArrayList<>();
        list.add(campaign.getCampaignName());
        list.add(campaign.getTotalImpression());
        list.add(campaign.getTotalClicks());
        list.add(campaign.getClickRate());
        list.add(campaign.getCostPerClick());
        list.add(campaign.getTotalAppInstall());
        list.add("=C2*E2");

        return list;
    }
}
