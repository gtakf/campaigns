package com.alictus.campaigns.constant;

public class FileConstants {

    private FileConstants() {
    }

    public static final String EXCEL_PATH = "src/main/resources/datasets/";
    public static final String DOT_SPLIT = "\\.";
    public static final String EXCEL_XLSX_TYPE = "xlsx";
    public static final String EXCEL_XLSX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

}
