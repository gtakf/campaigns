package com.alictus.campaigns.constant;


public class SheetsConstants {

    private SheetsConstants() {

    }

    public static final String VALUE_INPUT_OPTION = "USER_ENTERED";
    public static final String CAMPAIGN_RANGE = "A1:G2";

}
