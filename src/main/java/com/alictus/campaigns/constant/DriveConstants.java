package com.alictus.campaigns.constant;

public class DriveConstants {

    private DriveConstants() {

    }

    public static final String FOLDER_NAME = "datasets";
    public static final String FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
    public static final String SPREADSHEET_MIME_TYPE = "application/vnd.google-apps.spreadsheet";

}
