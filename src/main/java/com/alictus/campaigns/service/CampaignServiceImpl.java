package com.alictus.campaigns.service;

import com.alictus.campaigns.constant.DriveConstants;
import com.alictus.campaigns.constant.SheetsConstants;
import com.alictus.campaigns.domain.Campaign;
import com.alictus.campaigns.domain.ExcelFile;
import com.alictus.campaigns.mapper.CampaignMapper;
import com.alictus.campaigns.service.contract.*;
import com.google.api.services.drive.model.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CampaignServiceImpl implements CampaignService {

    private final static Logger logger = LogManager.getLogger(CampaignServiceImpl.class);
    private FileService fileService;
    private DriveService driveService;
    private SheetsService sheetsService;

    @Autowired
    public CampaignServiceImpl(FileService fileService, DriveService driveService, SheetsService sheetsService) {
        this.fileService = fileService;
        this.driveService = driveService;
        this.sheetsService = sheetsService;
    }

    //@EventListener(ApplicationReadyEvent.class)
    @Override
    public void manageDatasets() throws IOException {
        logger.debug("CampaignServiceImpl - manageDatasets - START");

        String localDatasetName = "dataset.xlsx";
        ExcelFile excelFile = fileService.readExcel(localDatasetName, 0);

        driveService.uploadFile(excelFile, null);
        File folder = driveService.createFolder(DriveConstants.FOLDER_NAME);
        driveService.uploadFile(excelFile, folder.getId());

        excelFile.getCells().remove(0);
        List<Campaign> campaigns = excelFile.getCells().values().stream().map(
                CampaignMapper.INSTANCE::toCampaignFromCells).collect(Collectors.toList());

        List<List<Object>> values = new ArrayList<>();
        List<Object> firstRow = new ArrayList<>();
        firstRow.add("Campaign name");
        firstRow.add("Total Impression");
        firstRow.add("Total Clicks");
        firstRow.add("CTR (%)");
        firstRow.add("CPC");
        firstRow.add("Total App Install");
        firstRow.add("Total budget");
        values.add(firstRow);

        for (Campaign campaign : campaigns) {
            File spreadSheet = driveService.createEmptySpreadSheet(campaign.getCampaignName(), folder.getId());
            if (values.size() > 1) {
                values.remove(1);
            }
            List<Object> secondRow = CampaignMapper.INSTANCE.toSpreadSheetsRowFromCampaign(campaign);
            values.add(secondRow);
            sheetsService.updateSpreadSheet(spreadSheet.getId(), SheetsConstants.CAMPAIGN_RANGE, values);
        }

        logger.debug("CampaignServiceImpl - manageDatasets - END");
    }

    @Override
    public Campaign readCampaignFromRemote(String campaignName) throws IOException {
        logger.debug("CampaignServiceImpl - readCampaignFromRemote - START");

        File spreadSheetFile = driveService.getFile(campaignName, DriveConstants.FOLDER_NAME, DriveConstants.SPREADSHEET_MIME_TYPE);
        List<List<Object>> values = sheetsService.readSpreadSheet(spreadSheetFile.getId(), SheetsConstants.CAMPAIGN_RANGE);
        Campaign campaign = CampaignMapper.INSTANCE.toCampaignFromSpreadSheet(values.get(1));

        logger.debug("CampaignServiceImpl - readCampaignFromRemote - END");
        return campaign;
    }

}
