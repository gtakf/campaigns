package com.alictus.campaigns.service;

import com.alictus.campaigns.domain.Campaign;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.File;
import java.util.List;

public class JavaFxPlotter {

    public static File toLineChart(String imageName, List<Campaign> campaignList) {

        File chartFile = new File(imageName);

        // results: {completed, successful}
        Boolean[] results = new Boolean[]{false, false};

        SwingUtilities.invokeLater(() -> {

            // Initialize FX Toolkit
            new JFXPanel();

            Platform.runLater(() -> {
                final CategoryAxis xAxis = new CategoryAxis();
                xAxis.setLabel("Campaign Names");
                final NumberAxis yAxis = new NumberAxis();
                yAxis.setLabel("Values");

                LineChart totalImpressionLineChart = new LineChart(xAxis, yAxis);
                XYChart.Series totalImpressionSeries = new XYChart.Series();
                totalImpressionSeries.setName("Total Impression");
                for (Campaign campaign : campaignList) {
                    totalImpressionSeries.getData().add(new XYChart.Data(campaign.getCampaignName(), campaign.getTotalImpression()));
                }
                totalImpressionLineChart.getData().add(totalImpressionSeries);

                Scene scene = new Scene(totalImpressionLineChart, 800, 600);

                WritableImage image = scene.snapshot(null);

                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", chartFile);
                    results[1] = true;
                } catch (Exception e) {
                    results[0] = true;
                } finally {
                    results[0] = true;
                }
            });
        });

        while (!results[0]) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return (results[1]) ? chartFile : null;
    }

}
