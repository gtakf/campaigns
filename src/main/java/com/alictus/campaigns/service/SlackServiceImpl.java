package com.alictus.campaigns.service;

import com.alictus.campaigns.domain.Campaign;
import com.alictus.campaigns.service.contract.CampaignService;
import com.alictus.campaigns.service.contract.SlackService;
import com.slack.api.bolt.App;
import com.slack.api.bolt.AppConfig;
import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.bolt.socket_mode.SocketModeApp;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.files.FilesUploadRequest;
import com.slack.api.model.block.ImageBlock;
import com.slack.api.model.block.LayoutBlock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.util.ajax.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class SlackServiceImpl implements SlackService {

    private static final Logger logger = LogManager.getLogger(FileServiceImpl.class);

    @Value("${slack.bot.token}")
    private String slackBotToken;

    @Value("${slack.app.token}")
    private String slackAppToken;

    private CampaignService campaignService;
    private App app;

    @Autowired
    public SlackServiceImpl(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void startSlackServer() throws Exception {
        logger.debug("SlackServiceImpl - startSlackServer - START");
        this.app = new App(AppConfig.builder().singleTeamBotToken(slackBotToken).build());

        app.command("/hello", (req, ctx) -> {
            return ctx.ack(":wave: Hello Mahmut Tuncer Show!");
        });

        app.command("/calculate_budget", (this::calculateBudget));

        app.command("/compare_campaigns", (this::compareCampaigns));

        SocketModeApp socketModeApp = new SocketModeApp(slackAppToken, app);
        socketModeApp.start();
        logger.debug("SlackServiceImpl - startSlackServer - END");
    }

    private Response compareCampaigns(SlashCommandRequest req, SlashCommandContext ctx) {
        logger.debug("SlackServiceImpl - compareCampaigns - START");
        Response response = null;

        String[] campaignNames = ((HashMap<String, String>) JSON.parse(req.getRequestBodyAsString())).get("text").split(" ");

        List<Campaign> campaignList = new ArrayList<>();
        for (String campaignName : campaignNames) {
            try {
                Campaign campaign = campaignService.readCampaignFromRemote(campaignName);
                campaignList.add(campaign);
            } catch (IOException e) {
                logger.error("SlackServiceImpl - compareCampaigns - Error : {}", e.getMessage());
                response = ctx.ack(":wave: Campaign with name " + campaignName + " is not found!");
                return response;
            }
        }

        response = createChart(ctx, campaignList);
        if (response != null) {
            return response;
        }

        List<LayoutBlock> blocks = new ArrayList<>();
        ImageBlock imageBlock = new ImageBlock();
        imageBlock.setImageUrl("some path");
        blocks.add(imageBlock);
        response = ctx.ack(blocks);

        logger.debug("SlackServiceImpl - compareCampaigns - END");
        return response;
    }

    private Response createChart(SlashCommandContext ctx, List<Campaign> campaignList) {
        Response response;

        JavaFxPlotter.toLineChart("chart.png", campaignList);
        File chartImage = new File("chart.png");
        InputStream chartStream;
        try {
            chartStream = new FileInputStream(chartImage);
        } catch (FileNotFoundException e) {
            logger.error("SlackServiceImpl - compareCampaigns - Error : {}", e.getMessage());
            response = ctx.ack(":wave: FileNotFoundException while reading image - " + e.getMessage());
            return response;
        }

        FilesUploadRequest chartImageRequest;
        try {
            chartImageRequest = FilesUploadRequest.builder()
                    .token(slackBotToken)
                    .file(chartImage)
                    .filename("chart.png")
                    .filetype("png")
                    .fileData(chartStream.readAllBytes())
                    .build();
        } catch (IOException e) {
            logger.error("SlackServiceImpl - compareCampaigns - Error : {}", e.getMessage());
            response = ctx.ack(":wave: IOException while reading image - " + e.getMessage());
            return response;
        }
        try {
            app.client().filesUpload(chartImageRequest);
        } catch (IOException e) {
            logger.error("SlackServiceImpl - compareCampaigns - Error : {}", e.getMessage());
            response = ctx.ack(":wave: IOException while uploading image - " + e.getMessage());
            return response;
        } catch (SlackApiException e) {
            logger.error("SlackServiceImpl - compareCampaigns - Error : {}", e.getMessage());
            response = ctx.ack(":wave: SlackApiException - " + e.getMessage());
            return response;
        }

        return null;
    }

    private Response calculateBudget(SlashCommandRequest req, SlashCommandContext ctx) {
        logger.debug("SlackServiceImpl - calculateBudget - START");
        String campaignName = ((HashMap<String, String>) JSON.parse(req.getRequestBodyAsString())).get("text");

        Response response;
        try {
            Campaign campaign = campaignService.readCampaignFromRemote(campaignName);
            response = ctx.ack(":wave: Total budget for campaign with name " + campaignName + " is " + campaign.getTotalBudget());
        } catch (Exception e) {
            logger.error("SlackServiceImpl - calculateBudget - Error : {}", e.getMessage());
            response = ctx.ack(":wave: Campaign with name " + campaignName + " is not found!");
        }

        logger.debug("SlackServiceImpl - calculateBudget - END");
        return response;
    }

}
