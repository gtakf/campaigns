package com.alictus.campaigns.service;

import com.alictus.campaigns.constant.DriveConstants;
import com.alictus.campaigns.constant.GoogleConstants;
import com.alictus.campaigns.domain.ExcelFile;
import com.alictus.campaigns.service.contract.DriveService;

import com.alictus.campaigns.service.contract.GoogleService;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;


@Service
public class DriveServiceImpl implements DriveService {

    private static final Logger logger = LogManager.getLogger(FileServiceImpl.class);
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private GoogleService googleService;
    private Drive service;

    @Autowired
    public DriveServiceImpl(GoogleService googleService) throws GeneralSecurityException, IOException {
        this.googleService = googleService;
        this.service = getDrive();
    }

    @Override
    public File uploadFile(ExcelFile excelFile, String folderId) throws IOException {
        logger.debug("DriveServiceImpl - uploadFile - START");
        File fileMetadata = new File();
        fileMetadata.setName(excelFile.getFilename());
        if (folderId != null) {
            fileMetadata.setParents(Collections.singletonList(folderId));
        }
        FileContent mediaContent = new FileContent(excelFile.getMimeType(), excelFile.getFile());
        File file = service.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();

        logger.debug("DriveServiceImpl - uploadFile - END");
        return file;
    }

    @Override
    public File createFolder(String name) throws IOException {
        logger.debug("DriveServiceImpl - createFolder - START");

        File fileMetadata = new File();
        fileMetadata.setName(name);
        fileMetadata.setMimeType(DriveConstants.FOLDER_MIME_TYPE);

        File file = service.files().create(fileMetadata)
                .setFields("id")
                .execute();

        logger.debug("DriveServiceImpl - createFolder - END");
        return file;
    }

    @Override
    public File createEmptySpreadSheet(String fileName, String folderId) throws IOException {
        logger.debug("DriveServiceImpl - createEmptySpreadSheet - START");
        File fileMetadata = new File();
        fileMetadata.setName(fileName);
        if (folderId != null) {
            fileMetadata.setParents(Collections.singletonList(folderId));
        }
        fileMetadata.setMimeType(DriveConstants.SPREADSHEET_MIME_TYPE);
        File file = service.files().create(fileMetadata)
                .setFields("id")
                .execute();

        logger.debug("DriveServiceImpl - createEmptySpreadSheet - END");
        return file;
    }

    @Override
    public File getFile(String fileName, String folderName, String mimeType) throws IOException {
        logger.debug("DriveServiceImpl - getFile - START");

        File folder = service.files().list()
                .setQ("mimeType='" + DriveConstants.FOLDER_MIME_TYPE + "'" +
                        " and name='" + folderName + "'")
                .setSpaces("drive")
                .execute().getFiles().get(0);

        File file = service.files().list()
                .setQ("mimeType='" + mimeType + "'" +
                        " and name='" + fileName + "'" +
                        " and '" + folder.getId() + "' in parents")
                .setSpaces("drive")
                .execute().getFiles().get(0);

        logger.debug("DriveServiceImpl - getFile - END");
        return file;
    }

    private Drive getDrive() throws GeneralSecurityException, IOException {
        logger.debug("DriveServiceImpl - getDrive - START");
        final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleService.getCredentials(HTTP_TRANSPORT, SCOPES))
                .setApplicationName(GoogleConstants.APPLICATION_NAME)
                .build();
        logger.debug("DriveServiceImpl - getDrive - END");
        return service;
    }

}
