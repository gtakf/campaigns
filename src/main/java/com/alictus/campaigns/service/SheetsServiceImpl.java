package com.alictus.campaigns.service;

import com.alictus.campaigns.constant.GoogleConstants;
import com.alictus.campaigns.constant.SheetsConstants;
import com.alictus.campaigns.service.contract.GoogleService;
import com.alictus.campaigns.service.contract.SheetsService;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@Service
public class SheetsServiceImpl implements SheetsService {
    private static final Logger logger = LogManager.getLogger(FileServiceImpl.class);
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private GoogleService googleService;
    private Sheets service;

    @Autowired
    public SheetsServiceImpl(GoogleService googleService) throws GeneralSecurityException, IOException {
        this.googleService = googleService;
        this.service = getSheets();
    }

    @Override
    public Spreadsheet createSpreadSheet(String name, String folderId) throws IOException {
        logger.debug("SheetsServiceImpl - createSpreadSheet - START");
        Spreadsheet spreadsheet = new Spreadsheet().setProperties(new SpreadsheetProperties().setTitle(name));
        spreadsheet = service.spreadsheets().create(spreadsheet)
                .setFields("spreadsheetId")
                .execute();

        logger.debug("SheetsServiceImpl - createSpreadSheet - END");
        return spreadsheet;
    }

    @Override
    public UpdateValuesResponse updateSpreadSheet(String spreadsheetId, String range,
                                                  List<List<Object>> values) throws IOException {
        logger.debug("SheetsServiceImpl - updateSpreadSheet - START");

        ValueRange body = new ValueRange().setValues(values);
        UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, range, body)
                .setValueInputOption(SheetsConstants.VALUE_INPUT_OPTION)
                .execute();

        logger.debug("SheetsServiceImpl - updateSpreadSheet - END");
        return result;
    }

    @Override
    public List<List<Object>> readSpreadSheet(String spreadsheetId, String range) throws IOException {
        logger.debug("SheetsServiceImpl - readSpreadSheet - START");

        ValueRange result = service.spreadsheets().values().get(spreadsheetId, range).execute();

        logger.debug("SheetsServiceImpl - readSpreadSheet - END");
        return result.getValues();
    }

    private Sheets getSheets() throws GeneralSecurityException, IOException {
        logger.debug("SheetsServiceImpl - getSheets - START");
        final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleService.getCredentials(HTTP_TRANSPORT, SCOPES))
                .setApplicationName(GoogleConstants.APPLICATION_NAME)
                .build();
        logger.debug("SheetsServiceImpl - getSheets - END");
        return service;
    }

}
