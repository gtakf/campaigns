package com.alictus.campaigns.service.contract;

import com.alictus.campaigns.domain.Campaign;

import java.io.IOException;

public interface CampaignService {

    void manageDatasets() throws IOException;

    Campaign readCampaignFromRemote(String campaignName) throws IOException;

}
