package com.alictus.campaigns.service.contract;


import com.alictus.campaigns.domain.ExcelFile;
import com.google.api.services.drive.model.File;

import java.io.IOException;

public interface DriveService {

    File uploadFile(ExcelFile excelFile, String folderId) throws IOException;

    File createFolder(String name) throws IOException;

    File createEmptySpreadSheet(String fileName, String folderId) throws IOException;

    File getFile(String fileName, String folderName, String mimeType) throws IOException;
}
