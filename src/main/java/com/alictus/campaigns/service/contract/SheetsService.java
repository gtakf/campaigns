package com.alictus.campaigns.service.contract;

import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;

import java.io.IOException;
import java.util.List;

public interface SheetsService {

    Spreadsheet createSpreadSheet(String name, String folderId) throws IOException;

    UpdateValuesResponse updateSpreadSheet(String spreadsheetId, String range, List<List<Object>> values) throws IOException;

    List<List<Object>> readSpreadSheet(String spreadsheetId, String range) throws IOException;
}