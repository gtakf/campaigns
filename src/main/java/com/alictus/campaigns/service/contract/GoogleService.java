package com.alictus.campaigns.service.contract;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.IOException;
import java.util.List;

public interface GoogleService {

    Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, List<String> scopes) throws IOException;
}
