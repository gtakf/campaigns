package com.alictus.campaigns.service.contract;

import com.alictus.campaigns.domain.ExcelFile;

import java.io.IOException;

public interface FileService {

    ExcelFile readExcel(String fileName, int sheetIndex) throws IOException;

}
