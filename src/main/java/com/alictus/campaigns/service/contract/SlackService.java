package com.alictus.campaigns.service.contract;

public interface SlackService {

    void startSlackServer() throws Exception;

}
