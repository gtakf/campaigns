package com.alictus.campaigns.service;

import com.alictus.campaigns.constant.FileConstants;
import com.alictus.campaigns.domain.ExcelFile;
import com.alictus.campaigns.service.contract.FileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class FileServiceImpl implements FileService {

    private final static Logger logger = LogManager.getLogger(FileServiceImpl.class);

    @Override
    public ExcelFile readExcel(String fileName, int sheetIndex) throws IOException {
        logger.debug("FileServiceImpl - readExcel - START");
        ExcelFile excelFile = new ExcelFile();
        File file = new File(FileConstants.EXCEL_PATH + fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        boolean isXLSX = fileName.split(FileConstants.DOT_SPLIT)[1].equalsIgnoreCase(FileConstants.EXCEL_XLSX_TYPE);
        Map<Integer, List<String>> data = new HashMap<>();
        if (isXLSX) {
            data = readExcelXLSX(fileInputStream, sheetIndex);
            excelFile.setMimeType(FileConstants.EXCEL_XLSX_MIME_TYPE);
        } else {

        }
        excelFile.setFilename(fileName);
        excelFile.setSize(file.length());
        excelFile.setCells(data);
        excelFile.setFile(file);
        logger.debug("FileServiceImpl - readExcel - END");
        return excelFile;
    }

    private Map<Integer, List<String>> readExcelXLSX(FileInputStream fileInputStream, int sheetIndex) throws IOException {
        logger.debug("FileServiceImpl - readExcelXLSX - START");
        Workbook workbook = new XSSFWorkbook(fileInputStream);
        Sheet sheet = workbook.getSheetAt(sheetIndex);
        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<>());
            for (Cell cell : row) {
                switch (cell.getCellType().name()) {
                    case "NUMERIC":
                        data.get(i).add(String.valueOf(cell.getNumericCellValue()));
                        break;
                    case "STRING":
                        data.get(i).add(cell.getStringCellValue());
                        break;
                    default:
                        data.get(i).add("");
                        break;
                }
            }
            i++;
        }

        logger.debug("FileServiceImpl - readExcelXLSX - END");
        return data;
    }

}
